const index = require("../index"); // Import index to run rl on this file

// Function to calculate rectangular prism volume
function calculateVolumeRectangular(length, width, height) {
  return length * width * height;
}

// Function to input the length
function input() {
  index.rl.question("Length: ", (length) => {
    index.rl.question("Width: ", (width) => {
      index.rl.question("Height: ", (height) => {
        if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
          console.log(`\nRectangular: ${calculateVolumeRectangular(length, width, height)}`);
          index.rl.close();
        } else {
          console.log("Input must be a number !\n");
          input();
        }
      });
    });
  });
}

module.exports = { input }; // Export the input, so the another file can run this code
