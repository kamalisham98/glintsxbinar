const index = require("../index"); // Import index to run rl on this file

function calculateConeVolume(radius,height) {
    let CircleArea = Math.PI * radius ** 2
    return CircleArea * height
}

// Function to input the radius and height

function input() {
    index.rl.question("radius: ", (radius) => {
        index.rl.question("height: ", (height) => {
            if (!isNaN(radius) && !isNaN(height)) {
                console.log(`\nCone: ${calculateConeVolume(radius, height)}`);
                index.rl.close();
              } else {
                console.log("Input must be a number !\n");
                input();
              }
            })
        })
    }
    
    module.exports = { input }; // Export the input, so the another file can run this code
      