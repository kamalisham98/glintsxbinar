const index = require("../index"); // Import index to run rl on this file

function calculateBallVolume(radius) {
    return 3/4 * Math.PI * radius ** 3
}
  
// Function to input the radius
function input() {
    index.rl.question("radius: ", (radius) => {
     
          if (!isNaN(radius)) {
            console.log(`\nBall: ${calculateBallVolume(radius)}`);
            index.rl.close();
          } else {
            console.log("Input must be a number !\n");
            input();
        }
    });
}

module.exports = { input }; // Export the input, so the another file can run this code