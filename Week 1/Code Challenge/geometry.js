const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin, 
  output: process.stdout,
});

// function of geometries

// function of rectangle
function rectangle(w, l, h) {
  let volume = w * l * h; //formula of volume
  return volume;
}

// function of cone
function cone(r, h) {
  let volume = (Math.PI * r ** 2 * h) / 3; //formula of volume
  return volume;
}

let RangleA = rectangle(4, 12, 6);
let RangleB = rectangle(10, 22, 3);
let RangleBrother = RangleA + RangleB;

console.log("\nrectangle A volume : " + RangleA + " cm3");
console.log("rectangle B volume : " + RangleB + " cm3");
console.log("sum of 3-dimensional volume is " + RangleBrother + " cm3");

let coneA = cone(7, 33);
let coneB = cone(14, 21);
let conetotal = coneA + coneB;
console.log("\nconeA volume : " + coneA + " cm3");
console.log("coneB volume : " + coneB + " cm3");
console.log("volum cone A + volum cone B = " + conetotal + " cm3\n");

//input way 1
function input() {
  rl.question("radius: ", (radius) => {
    rl.question("height: ", (height) => {
      if (!isNaN(radius) && !isNaN(height)) {
        console.log(cone(radius, height));
        rl.close();
      } else {
        console.log("radius and height must be number!");
        input();
      }
    });
  });
}
//
//Input Way2
function inputRadius() {
  rl.question("Radius: ", (radius) => {
    if (!isNaN(radius)) {
      inputHeight(radius);
    } else {
      console.log("radius must be in number");
      inputRadius();
    }
  });
}

function inputHeight(radius) {
  rl.question("Height: ", (height) => {
    if (!isNaN(height)) {
      console.log(`Cone volume's is ${cone(radius, height)} cm3`);
      rl.close();
    } else {
      console.log("height must be in number");
      inputHeight(radius);
    }
  });
}




console.log("----input value----");
// input(); //way 1
input(); // way 2'

