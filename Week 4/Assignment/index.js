const express = require("express");
const app = express();

// Import Routes
const barangRoutes = require("./routes/barangRoute");
const pelangganRoutes = require("./routes/pelangganRoute");
// Use to read req.body
app.use(express.urlencoded({ extended: true }));

app.use("/barang", barangRoutes);
app.use("/pelanggan", pelangganRoutes); // Added

//server running on port 3000
app.listen(3000, () => console.log("Server running on 3000"));
