const connection = require("../models");

// Get all data from barang
const getAll = (req, res) => {
  let sqlGetAll =
    "SELECT b.id, b.nama, b.harga,p.nama as pemasok  FROM barang b join pemasok p on p.id = b.id_pemasok";

  connection.query(sqlGetAll, (err, result) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Sever Error",
        error: err,
      });
    }
    // If success
    return res.status(200).json({
      message: "Success",
      data: result,
    });
  });
};

//Get one
const getOne = (req, res) => {
  let sqlGetOne =
    "SELECT b.id,b.nama, b.harga, p.nama as pemasok  FROM barang b join pemasok p on b.id_pemasok = p.id WHERE b.id = ?";

  //Run query
  connection.query(sqlGetOne, [req.params.id], (err, result) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Sever Error",
        error: err,
      });
    }
    // if success
    return res.status(201).json({
      message: "success",
      data: result[0],
    });
  });
};

const post = (req, res) => {
  let sqlPost = `INSERT INTO barang (nama, harga, id_pemasok) VALUES (${req.body.nama},${req.body.harga},${req.body.id_pemasok}) `;

  connection.query(sqlPost, (err, result) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Sever Error",
        error: err,
      });
    }
    // If success
    let sqlSelect = `SELECT b.id,b.nama, b.harga, p.nama as pemasok  FROM barang b join pemasok p on b.id_pemasok = p.id WHERE b.id = ${result.insertId}`;

    connection.query(sqlSelect, (err, result) => {
      // If error
      if (err) {
        return res.status(500).json({
          message: "Internal Sever Error",
          error: err,
        });
      }
      // if success
      return res.status(201).json({
        message: "success",
        data: result[0],
      });
    });
  });
};

// Delete data
const deleteData = (req, res) => {
  let sqlDelete = `DELETE FROM barang WHERE id = ${req.params.id} `;

  connection.query(sqlDelete, (err, result) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Sever Error",
        error: err,
      });
    }
    return res.status(410).json({
      message: "Success",
    });
  });
};

//Update data

const update = (req, res) => {
  let sqlUpdate = `UPDATE barang SET nama=${req.body.nama}, harga =${req.body.harga}, id_pemasok = ${req.body.id_pemasok} WHERE id = ${req.params.id}`;

  connection.query(sqlUpdate, (err, result) => {
    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Sever Error",
        error: err,
      });
    }
    // If success
    let sqlGetOne =
      "SELECT b.id,b.nama, b.harga, p.nama as pemasok  FROM barang b join pemasok p on b.id_pemasok = p.id WHERE b.id = ?";

    //Run query
    connection.query(sqlGetOne, [req.params.id], (err, result) => {
      // If error
      if (err) {
        return res.status(500).json({
          message: "Internal Sever Error",
          error: err,
        });
      }
      // if success
      return res.status(201).json({
        message: "success",
        data: result[0],
      });
    });
  });
};

module.exports = { getAll, getOne, post, deleteData, update };
