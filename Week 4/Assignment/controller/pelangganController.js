const connection = require("../models");

// Create Using Promise
const getAll = (req, res) => {
  let sqlGetAll = "SELECT id, nama FROM pelanggan";

  connection.query(sqlGetAll, (err, result) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Sever Error",
        error: err,
      });
    }
    return res.status(200).json({
      message: "Success",
      data: result,
    });
  });
};

const getOne = (req, res) => {
  let sqlGetOne = `SELECT id, nama FROM pelanggan WHERE ${req.params.id}`;

  connection.query(sqlGetOne, (err, result) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Sever Error",
        error: err,
      });
    }
    return res.status(200).json({
      message: "Success",
      data: result[0],
    });
  });
};

const create = (req, res) => {
  let sqlCreate = `INSERT INTO pelanggan (nama) VALUES (${req.body.nama})`;

  connection.query(sqlCreate, (err, result) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Sever Error",
        error: err,
      });
    }
    let sqlSelect = `SELECT id,nama FROM pelanggan WHERE id = ${result.insertId}`;

    connection.query(sqlSelect, (err, result) => {
      if (err) {
        return res.status(500).json({
          message: "Internal Sever Error",
          error: err,
        });
      }
      return res.status(200).json({
        message: "Success",
        data: result[0],
      });
    });
  });
};

const updateData = (req, res) => {
  let sqlUpdate = `UPDATE pelanggan SET nama = ${req.body.nama} WHERE id =${req.params.id}`;

  connection.query(sqlUpdate, (err, result) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    let sqlSelect = `SELECT * FROM pelanggan WHERE id = ${req.params.id}`;

    connection.query(sqlSelect, (err, result) => {
      if (err) {
        return res.status(500).json({
          message: "Internal Sever Error",
          error: err,
        });
      }
      return res.status(200).json({
        message: "Success",
        data: result[0],
      });
    });
  });
};

const deleteData = (req, res) => {
  let sqlDelete = `DELETE FROM pelanggan WHERE id = ${req.params.id}`;

  connection.query(sqlDelete, (err, result) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Sever Error",
        error: err,
      });
    }
    return res.status(201).json({
      message: "Success",
    });
  });
};

module.exports = { getAll, getOne, create, updateData,deleteData };
