const express = require("express");
const router = express.Router();

const barangController = require("../controller/barangController");

router.get("/", barangController.getAll);
router.get("/:id", barangController.getOne);
router.post("/", barangController.post);
router.delete("/:id", barangController.deleteData);
router.put("/:id", barangController.update);

module.exports = router;
