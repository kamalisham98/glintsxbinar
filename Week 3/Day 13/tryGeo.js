const { Square, Rectangle, Triangle, Beam,Cube, Tube,Cone } = require("./geometry");


// let Square1 = new Square(10);
// let rectangle1 = new Rectangle(5,5);
// let triangle1 = new Triangle(12,10);
let beam1= new Beam(7,10,4);
let cube1= new Cube(7);
let Tube1= new Tube(7,14);
let cone1= new Cone(7,20);

beam1.calculateVolume();
cube1.calculateVolume();
Tube1.calculateVolume();
cone1.calculateVolume();
