const Square = require("./square");
const Rectangle = require("./rectangle");
const Triangle = require("./triangle");
const Beam = require("./beam");
const Cube = require("./Cube");
const Tube = require("./Tube");
const Cone = require("./Cone");


module.exports = { Square, Rectangle, Triangle, Beam,Cube, Tube,Cone };
