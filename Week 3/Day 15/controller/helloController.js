class HelloController{
    get(req, res) {
        console.log("repons to termnial"); // return to terminal
        console.log("respons to terminal 2");
        // res.send(`respons for  Postman is ${req.query.name}`);  //return to postman
        // -------------------------------------
        // req.params={name : "kamal", fullname:"Kamal Isham"} //manually add
        // res.send(`${req.params.name} fullname is ${req.params.fullname} `);
        // ---------------------------------------------
        if(req.params.name == "kamal"){
            res.send(`fullname is kamal isham`);
        }else{
            res.send(`user not found`);
        }
    }
    post(req, res) {
        console.log("Usually used to add data!");
        res.send("This is POST!");
    }
    put(req, res) {
        console.log("Usually used to update data!");
        res.send("This is PUT!");
    }
    delete(req, res) {
        console.log("Usually used to delete data!");
        res.send("This is DELETE!");
    }

}

module.exports = new HelloController;